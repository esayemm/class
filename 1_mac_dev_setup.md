# Mac development setup

The purpose of this document is to guide you through setting up a javascript
development environment on mac OS.

## Install Prerequisite Programs

#### Terminal

A terminal allows you to interact with the command line. The default terminal on
mac OS is simply called "terminal", but you can use iterm2 which is an alternative that provides
additional features.

- [iterm2](https://www.iterm2.com/)

#### Text Editor

A text editor allows you to edit text. These text editors specialize in editing
code and have features such as syntax highlighting (color coding code) and
autocomplete etc.

- [vsstudio](https://www.visualstudio.com/)


## Setup command line environment

First you have to run `xcode-select --install`, most things on the mac os
requires xcode to be installed so this command will install it.

If this is the first time you are interacting with the command line follow these
steps.

1. Open iterm2 from applications.
2. Inside iterm2 type `xcode-select --install`.
3. Press enter to execute the command.

You will need to start becoming more comfortable using the command line to
navigate your computer. Here are some helpful resources.

- http://mally.stanford.edu/~sr/computing/basic-unix.html
- https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html

Please read and familiarize yourself with these commands as they will become
essential.

#### Install homebrew

Homebrew is a package manager for mac os. You can think of a package manager
kind of like an app store for programs. You can install programs by using
homebrew, but first you have to install homebrew. Follow the instructions in the
link to install it.

- https://brew.sh/

## Set up nodejs environment

Now that brew is installed you can use it to set up a nodejs environment on your
computer.

Run the following command to install node.

```
brew install node
```

After the command finishes running you should now have a new program available
called node. To check that node is successfully installed you can run the
following command.


```
node --version
```

This should print the version of node you just installed.

## Homework

Go through this lesson on code academy about using the command line.

https://www.codecademy.com/learn/learn-the-command-line
https://www.youtube.com/watch?v=cX9ASUE3YAQ

## Set up project directory

Ok now lets setup a directory for you to store all your projects. Since you
already learned how to navigate your computer using the command line, do this
using the command line.

Lets make a directory named `projects` in your home directory.

*try it yourself before reading the answer*

```
# navigate to home directory
cd ~
# make a directory called projects
mkdir projects
# navigate into projects directory
cd projects
```
>>>>>>> 1_mac_dev_setup
